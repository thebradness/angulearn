import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SchoolChooserComponent} from './components/school-chooser/school-chooser.component';
import {StudentChooserComponent} from './components/student-chooser/student-chooser.component';
import {SortPipe} from './pipes/sort.pipe';
import {routing} from "./app.routing";
import { ChooserComponent } from './components/chooser/chooser.component';
import { StudentViewComponent } from './components/student-view/student-view.component';

@NgModule({
  declarations: [
    AppComponent,
    SchoolChooserComponent,
    StudentChooserComponent,
    SortPipe,
    ChooserComponent,
    StudentViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    routing
  ],
  providers: [
    StudentChooserComponent,
    SchoolChooserComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
