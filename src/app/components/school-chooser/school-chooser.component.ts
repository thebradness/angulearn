import {Component} from '@angular/core';
import {UniversityService} from "../../services/university.service";
import {AppComponent} from "../../app.component";

@Component({
  selector: 'school-chooser',
  templateUrl: './school-chooser.component.html',
  styleUrls: ['./school-chooser.component.less']
})
export class SchoolChooserComponent {

  schools;
  schoolStudentMap;

  constructor(
    private appComponent: AppComponent,
    private schoolService: UniversityService,
  ) {
    this.subscribeToSchools();
    this.schoolStudentMap = this.appComponent.appState;
  }

  enroll(students: any[]): void {
    this.addEnrolledStudents(students);
    this.deselect();
  }

  deselect(): void {
    const selectedSchool = this.getSelectedSchool();
    selectedSchool && (selectedSchool.isSelected = false);
  }

  undoEnroll(school: any): void {
    let students = this.getEnrolledStudents(school);

    students.forEach(s => {
      s.isHidden = false;
      setTimeout(() => s.isEnrolled = false, 300);
    });

    students.length = 0; //NOTE = clears the array this is referencing (setting to empty would just change the var)
  }

  getStudentCount(school: any): number {
    const students = this.getEnrolledStudents(school);
    return students && students.length;
  }

  getEnrolledStudents(school: any): any[] {
    return this.schoolStudentMap[school.name];
  }

  getSelectedSchool(): any {
    return this.schools.find(s => s.isSelected);
  }

  selectSchool(school): void {
    if (school.isSelected) {
      school.isSelected = false;
      return;
    }

    this.deselect();
    school.isSelected = true;
  }

  buildSchoolName(school): string {
    const name = school.name;
    const country = school.alpha_two_code;

    return `${name} (${country})`;
  }

  buildSchoolLinkDisplay(school): string {
    const linkName = school.domains[0];
    return linkName.charAt(0).toUpperCase() + linkName.slice(1);
  }

  private addEnrolledStudents(selectedStudents: any[]): void {
    const schoolName = this.getSelectedSchool().name;
    let schoolStudents = this.schoolStudentMap[schoolName];

    if (!schoolStudents) {
      this.schoolStudentMap[schoolName] = schoolStudents = [];
    }

    schoolStudents.push(...selectedStudents);
  }

  private subscribeToSchools(): void {
    this.schoolService.data
      .subscribe(schools => this.schools = schools);
  }
}
