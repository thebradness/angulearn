import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AppComponent} from "../../app.component";
import {StudentChooserComponent} from "../student-chooser/student-chooser.component";

@Component({
  selector: 'student-view',
  templateUrl: './student-view.component.html',
  styleUrls: ['./student-view.component.less']
})
export class StudentViewComponent {

  student;

  constructor(
    private route: ActivatedRoute,
    private appComponent: AppComponent,
    private studentChooser: StudentChooserComponent,
  ) {
    this.subscribeToRouteParams();
  }

  buildName(): string {
    const title = this.student.name.title;
    const first = this.student.name.first;
    const last = this.student.name.last;

    return `${title} ${first} ${last}`;
  }

  buildAddress(): string {
    const num = this.student.location.street.number;
    const street = this.student.location.street.name;

    return `${num} ${street}`;
  }

  buildLocation(): string {
    const city = this.student.location.city;
    const state = this.student.location.state;
    const zip = this.student.location.postcode;

    return `${city}, ${state} ${zip}`;
  }

  private subscribeToRouteParams(): void {
    this.route.paramMap.subscribe(params => {
      const studentId = params.get('studentId');
      this.student = this.studentChooser.students.find(s => s.login.uuid == studentId);
    });
  }
}
