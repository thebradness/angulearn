import {Component, OnInit, ViewChild} from '@angular/core';
import {StudentChooserComponent} from "../student-chooser/student-chooser.component";
import {SchoolChooserComponent} from "../school-chooser/school-chooser.component";

@Component({
  selector: 'app-chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.less']
})
export class ChooserComponent {

  @ViewChild(StudentChooserComponent, {static: true}) studentChooser: StudentChooserComponent;
  @ViewChild(SchoolChooserComponent, {static: true}) schoolChooser: SchoolChooserComponent;

  constructor() {
  }

  enroll(): void {
    const selectedStudents = this.studentChooser.getSelected();
    this.schoolChooser.enroll(selectedStudents); //NOTE = needs to happen before student enroll b/c they get deselected
    this.studentChooser.enroll();
  }

  buildEnrollButtonDisplay(): string {
    const count = this.studentChooser.getSelectedCount();
    const plural = this.plural('Student', count);

    return `Enroll ${count} ${plural}`;
  }

  hasValidSelection(): boolean {
    const hasStudents = this.studentChooser.getSelectedCount();
    const hasSchool = this.schoolChooser.getSelectedSchool();

    return hasStudents && hasSchool;
  }

  private plural(word: string, count: number): string {
    if (count == 1) {
      return word;
    }

    return word + 's';
  }

}
