import {Component} from '@angular/core';
import {StudentService} from "../../services/student.service";

@Component({
  selector: 'student-chooser',
  templateUrl: './student-chooser.component.html',
  styleUrls: ['./student-chooser.component.less']
})
export class StudentChooserComponent {

  students;

  constructor(
    private studentService: StudentService,
  ) {
    this.subscribeToStudents();
  }

  enroll(): void {
    this.getSelected().forEach(s => {
      s.isEnrolled = true;
      s.isSelected = false;
      setTimeout(() => s.isHidden = true, 325);
    });
  }

  getSelectedCount(): number {
    return this.getSelected().length;
  }

  getSelected(): any[] {
    return this.students.filter(s => s.isSelected);
  }

  buildNameLine(student): string {
    const first = student.name.first;
    const last = student.name.last;
    const age = student.dob.age;

    return `${first} ${last} (${age})`;
  }

  buildAddressLine(student): string {
    const loc = student.location;
    return `${loc.city}, ${loc.state} ${loc.country}`;
  }

  private subscribeToStudents(): void {
    this.studentService.data
      .subscribe(students => {
        this.students = students;
        this.students.forEach(s => s.sortName = s.name.last);
      });
  }
}
