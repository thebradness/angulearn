import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  appState = {}; //NOTE = we can add things at sub levels later, this is just to show that we can do it
  title = 'Admissions Counselor';

}
