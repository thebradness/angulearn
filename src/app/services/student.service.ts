import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";

@Injectable({providedIn: 'root'})
export class StudentService {
  private BASE = 'https://randomuser.me/api/';
  private SEED = 'brad';
  private RESULT_COUNT = 5; //NOTE = not using static b/c you have to use StudentService.SEED

  data;
  private dataSource;

  constructor(private http: HttpClient) {
    this.vars();
    this.fetch();
  }

  vars(): void {
    this.dataSource = new BehaviorSubject([]); //NOTE = give empty array in case the html page picks it up too quick
    this.data = this.dataSource.asObservable();
  }

  fetch(): void {
    const url = `${this.BASE}?seed=${this.SEED}&results=${this.RESULT_COUNT}`;

    this.http
      .get(url)
      .subscribe((result: any) => this.dataSource.next(result.results)); //NOTE = give the result a type to use ".results" and to keep the ugly at the bottom
  }
}
