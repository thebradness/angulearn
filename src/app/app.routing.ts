import {RouterModule, Routes} from "@angular/router";
import {ChooserComponent} from "./components/chooser/chooser.component";
import {StudentViewComponent} from "./components/student-view/student-view.component";

const appRoutes: Routes = [
  {
    path: '',
    component: ChooserComponent
  },
  {
    path: 'student/:studentId',
    component: StudentViewComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
